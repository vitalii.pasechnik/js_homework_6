"use strict";


/*1. Опишіть своїми словами, що таке екранування, і навіщо воно потрібне в мовах програмування

	Экранирование нужно для правильного прочтения строк интерпретатором. Все спецсимволы, которые мы хотим вставиться в строку,
	начинаются со знака экранирования "\". Также экранирование используется если мы хотим добавить кавычки во внутрь строки, такие же как те,
	которые оборачивают эту строку. Если не экранировать такие кавычки в середине строки, интерпретатор определит конец строки, когда дойдет до второй кавычки.   
*/

/* 2. Які засоби оголошення функцій ви знаєте?

		Существует 3 основных способа - это function declaration, function expression и стрелочные функции. 
	1) function declaration - мы объявляем функцию с помощью ключевого слова function. Такая функция прочитывается интерпретатором до выполнения основного кода, 
	   и ее можно вызвать в любом месте кода, до ее объявления.
	2) function expression - такая функция объявляется как переменная спомощью ключевых слов let или const, создается так называемое функциональное выражение.
	   Такую функцию как и обычные переменные можно вызывать и использовать только после ее объявления. 
	3) Стрелочные функции это синтаксис ES6. Стрелочные функции не имеют имени, их можно присваивать переменным. Если выражение занимает 1 строку, то стрелка =>
	   заменяет слово return и возвращает резулютат вычислений. Если функционал занимает несколько строк, то он берется в фигурные скобки и для возврата резулютата, 
	   в этом случае нужно вызвать return.
*/

/* 3. Що таке hoisting, як він працює для змінних та функцій?

	hoisting - это так называемое "поднятие" объявления функций и переменных. Это когда мы можем вызывать функцию объявленную методом function declaration, 
	до места ее объявления в коде. Это также работает для глобальных переменных объявленных через var, но не работает для const и let. 
*/



function createAnotherUser(firstName = 'Empty', lastName = 'Empty', birthday = 'Empty') {

	const newUser = {

		firstName: prompt("Введіть ваше ім'я", "Ivan") || firstName,
		lastName: prompt("Введіть ваше прізвище", "Petrenko") || lastName,
		birthday: prompt("Введіть дату народження у форматі - дд.мм.рррр", "01.01.2000") || birthday,

		setFirstName(newFName) {
			if (/\d/g.test(newFName)) {
				throw new Error("Ви ввели не корректне ім'я!");
			} else {
				Object.defineProperty(this, 'firstName', {
					value: firstName.trim() || firstName,
				});
			}
		},

		setLastName(newFName) {
			if (/\d/g.test(newFName)) {
				throw new Error("Ви ввели не корректне прізвище!");
			} else {
				Object.defineProperty(this, 'lastName', {
					value: lastName.trim() || lastName,
				});
			}
		},

		getLogin() {
			if (this.firstName !== 'Empty' && this.lastName !== 'Empty') {
				return `${this.firstName[0].toLowerCase()}${this.lastName.toLowerCase()}`;
			} else {
				throw new Error("Щоб отримати логін введіть корректно ім'я та прізвище!");
			}
		},

		getAge() {
			if (this.birthday !== 'Empty') {
				const birthdayISO = `${this.birthday.slice(3, 5)}.${this.birthday.slice(0, 2)}.${this.birthday.slice(-4)}`;
				return Math.floor((new Date().getTime() - new Date(birthdayISO)) / (24 * 3600 * 365.25 * 1000));
			} else {
				throw new Error("Щоб отримати вік введіть корректно дату народження!");
			}
		},

		getPassword() {
			if (this.firstName !== 'Empty' && this.lastName !== 'Empty' && this.birthday !== 'Empty') {
				return `${this.firstName[0].toUpperCase()}${this.lastName.toLowerCase()}${this.birthday.slice(-4)}`;
			} else {
				throw new Error("Щоб отримати пароль введіть корректно всі данні!");
			}
		}
	}

	while (/\d/g.test(newUser.firstName) || /\d/g.test(newUser.lastName)) {

		if (/\d/g.test(newUser.firstName)) {
			alert("Ви ввели не корректне ім'я!");
			newUser.firstName = prompt("Спробуйте ще раз ввести ім'я") || firstName;
		};

		if (/\d/g.test(newUser.lastName)) {
			alert("Ви ввели не корректне прізвище!");
			newUser.lastName = prompt("Спробуйте ще раз ввести прізвище") || lastName;
		};
	}

	while (newUser.birthday !== "Empty" && (
		!/(\d{2}\.){2}\d{4}/g.test(newUser.birthday) ||
		+newUser.birthday.slice(0, 2) > 31 ||
		+newUser.birthday.slice(3, 5) > 12 ||
		(/\.02\./.test(newUser.birthday) && +newUser.birthday.slice(0, 2) > 29)
	)) {
		alert("Ви ввели не корректну дату!");
		newUser.birthday = prompt("Введіть дату народження у форматі - дд.мм.рррр", "01.01.2000") || birthday;
	}

	Object.defineProperties(newUser, {
		'firstName': {
			writable: false,
			configurable: true,
		},
		'lastName': {
			writable: false,
			configurable: true,
		},
		'getLogin': {
			writable: false,
			configurable: false,
		},
		'setLastName': {
			writable: false,
			enumerable: false
		},
		'setFirstName': {
			writable: false,
			enumerable: false
		},
	});
	return newUser;
};

const newUser = createAnotherUser();
console.log(newUser);
console.log(Object.entries(newUser));
console.log(`Ваш логін: ${newUser.getLogin()}`);
console.log(`Ваш вік: ${newUser.getAge()} років`);
console.log(`Ваш пароль: ${newUser.getPassword()}`);

// console.log('__TEST_1__');
// newUser.firstName = "Martin";
// newUser.lastName = "Perez";
// console.log(newUser);

// console.log('__TEST_2__');
// newUser.setFirstName("123_test");
// newUser.setLastName("Perez");
// console.log(newUser_2);

// console.log('__TEST_3__');
// newUser.setFirstName(" ");
// console.log(newUser_2);
// console.log(Object.entries(newUser_2));
// console.log(`Логін: ${newUser_2.getLogin()}`);
